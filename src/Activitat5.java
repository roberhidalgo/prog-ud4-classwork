public class Activitat5 {
    public static void main(String[] args) {
        mostrarTablaMultiplicar(5);
        mostrarTablaMultiplicar(9);
    }

    public static void mostrarTablaMultiplicar(int multiplicant) {
        System.out.println("Tabla del " + multiplicant);
        System.out.println("-----------");
        for (int i = 1; i <= 10; i++) {
            System.out.println(multiplicant + " * " + i + " = " + multiplicant * i);
        }
    }
}
