public class Activitat7 {

    public static void main(String[] args) {
        imprimirPotencia(10, 2);
        imprimirPotencia(10, -2);
        imprimirPotencia(2, 4);
        imprimirPotencia(2.5, 1);
    }

    public static void imprimirPotencia(double base, int exponente) {
        long resultado = calcularPotencia(base, exponente);
        System.out.printf("El resultado de %.1f^%d = %.3f\n",
                (float)base, exponente, (float)resultado);
    }

    public static long calcularPotencia(double base, int exponente) {
        return (long)Math.pow(base,exponente);
    }
}
