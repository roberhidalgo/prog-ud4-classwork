public class Activitat10 {

    public static void main(String[] args) {
        imprimeix('a', 20);
        imprimeix('@', 100);
        imprimeix('x', 12);
        imprimeix('3', 0);
    }

    public static void imprimeix(char caracter, int numVoltes){
        for (int i = 0; i < numVoltes; i++) {
            System.out.print(caracter);
        }
        System.out.println();
    }
}
