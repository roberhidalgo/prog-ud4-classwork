import java.util.Scanner;

public class Activitat4 {

    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);
        int numero1 = pedirNumero(teclado);
        int numero2 = pedirNumero(teclado);
        int numero3 = pedirNumero(teclado);

        int mayor = obtenerMayor(numero1, numero2, numero3);
        System.out.println("El mayor es el " + mayor);
    }

    public static int pedirNumero(Scanner teclado) {
        boolean esCorrecto;
        int numero = 0;
        do {
            if (teclado.hasNextInt()) {
                numero = teclado.nextInt();
                esCorrecto = true;
            } else {
                System.out.println("Escríbelo otra vez");
                esCorrecto = false;
            }

            // Limpiamos el buffer
            teclado.nextLine();
        } while (!esCorrecto);

        return numero;
    }

    public static int obtenerMayor(int num1, int num2) {
        if (num1 >= num2) {
            return num1;
        } else {
            return num2;
        }
    }

    public static int obtenerMayor(int numero1, int numero2, int numero3) {

        int maximo = obtenerMayor(numero1, numero2);
        return obtenerMayor(maximo, numero3);
    }
}
