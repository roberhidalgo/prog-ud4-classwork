public class Activitat12 {

    public static void main(String[] args) {
        imprimeixPiramidal('a', 5);
    }

    public static void imprimeixPiramidal(char caracter, int numLineas){
        for (int i = 1; i <= numLineas; i++) {
            for (int j = numLineas - i; j >= 1; j--) {
                System.out.print("  ");
            }
            for (int j = 1; j <= i; j++) {
                System.out.print(caracter + " ");
            }
            for (int j = 1; j < i; j++) {
                System.out.print(caracter + " ");
            }
            System.out.println();
        }
    }
}
