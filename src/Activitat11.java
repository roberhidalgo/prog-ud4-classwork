public class Activitat11 {
    public static void main(String[] args) {
        imprimeix('a', 30, 10);
        imprimeix('$', 10, 2);
    }

    public static void imprimeix(char caracter, int numVoltes, int numLinies){
        for (int i = 0; i < numLinies; i++) {
            Activitat10.imprimeix(caracter, numVoltes);
        }
    }
}
