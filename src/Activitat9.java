public class Activitat9 {

    public static void main(String[] args) {
        System.out.println(obtenirElMajor(3, 9, 10, 1));
    }
    public static int obtenirElMajor(int num1, int num2, int num3, int num4){
        int mayor1 = Activitat4.obtenerMayor(num1, num2);
        int mayor2 = Activitat4.obtenerMayor(num3, num4);

        return Activitat4.obtenerMayor(mayor1, mayor2);
    }
}
